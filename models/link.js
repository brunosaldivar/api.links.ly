var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.ObjectId;

var LinkModel = mongoose.model('Link', new mongoose.Schema(
{
	//_id: { type : ObjectId},
	idNote : { type: ObjectId, ref: 'Note'},
	tags: [String],
    description: String,
    largeUrl: String,
    shortUrl: String,
    lastVisitTime: Date,
    stars:	[{
    	_id: { type : ObjectId},
		idUser : { type : ObjectId, ref: 'User'},
    	number : Number,
    	dateCreated : { type: Date, default:  Date.now },
    }],

    dateCreated : { type: Date, default:  Date.now },
    dateDeleted :  { type: Date },
    deleted:	{ type: Boolean, default: 0 },
}));



exports.Link = LinkModel;

