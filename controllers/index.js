exports.chrome = require('./chrome.js').ChromeController;
exports.authentication = require('./authentication').AuthController;
exports.notes = require('./note').NotesController;
exports.url = require('./url').URLController;
