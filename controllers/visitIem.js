
var VisitItemModel = require('../models/index.js').VisitItem;

exports.VisitItem = (function (){

	return {

		save : function (item,userAgent, callback){

            var visit = new VisitItemModel({
            
                urlId	: item.urlId,
                chromeId : parseInt(item.id),
                referringVisitId:	item.referringVisitId,
                transition: item.transition, //agregar el tipo tabs
                visitId: item.visitId,
                visitTime: item.visitTime,
                visitDate : new Date(item.visitTime),
                tags : item.tags,
                userAgent: userAgent,
                                                        //stars: [],
            });

            visit.save(function (err){
                callback(err,visit);
            });
           // visit.save(function(err,data){if(err){ throw new Error(err); }});
        }
    }
})();

    