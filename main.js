var restify = require('restify');
var plugins = require('restify-plugins');
var bodyParser =    require("body-parser");
var mongoose = require('mongoose');
var socketio = require('socket.io')();
var fs = require('fs');

var routes = require('./routes.js');
var config = require('./config.js');
var Models = require('./models/index.js');
var controllers = require('./controllers/index.js');

var server = restify.createServer({
  name:   config.name,
});


console.log('API initialized') 
mongoose.connect(config.mongodb, { useMongoClient: true }).then(
  () => { console.log('MongoClient connected!') },
  err => { /** handle initial connection error */ });


server.use(plugins.acceptParser(server.acceptable));
server.use(plugins.queryParser());
server.use(plugins.bodyParser()); 
//server.use(restify.CORS()); 
server.use(
  function crossOrigin(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    return next();
  }
);

server.use(controllers.authentication.validate);
server.post('/login', routes.Auth.login);

//api.links.ly/v.1/note/
//server.post(config.version + '/note',routes.Notes.createNoteWithLinks);
server.post(config.version + '/chrome/addHistory',routes.Chrome.addHistory);
server.get(config.version + '/chrome/getHistories',routes.Chrome.getHistories);

var clients 	= [];
var io = socketio.listen(server.server); 
io.set("transports", ["polling","websocket"]);

io.use(controllers.authentication.wsValidate)
  .on('connection', function (sock) {

    clients.push(sock.id, sock.handshake.query.user.name, sock.handshake.query.user.id);

    console.log('connected: ' + sock.handshake.query.user.name);

    sock.on('tabs.getAllPaging', function (opt) {

      return new Promise(function(resolve, reject){
        
        controllers.url.getAllPaging(opt)
          .then(urls => {
            console.log('getAllPaging')
            console.dir(opt);
            console.log(urls.length)
            sock.emit('tabs.getAllPaging', {'data': urls})
          })
          .catch(err => {
            console.log('Error tabs.getAllPaging: ' + err);
          })
        });
      });
  

    sock.on('tabs.getAllCount',function(){
        controllers.url.getAllCount(function(e,d){
          console.log(d)
          sock.emit('tabs.getAllCount', {'count': d})
      });

    });
    
    sock.on('tabs.update', function (objInfo,cb) {
      console.log('sock.on:')
      controllers.url.add(objInfo,function(){
        console.log('save: ');
        console.dir(objInfo);
        if(cb)cb();
      });
    });
    sock.on('disconnect', function () {
      console.log('disconnect');
      clients.splice(sock.id);
    });
});

/*function uploadFile(req, res, next) {
    req.on('end', function (){
        fs.appendFile('./uploads/' + Date.now() + req.body.url + '.jpg',
         new Buffer(req.body.img.replace(/^data:image\/(png|gif|jpeg);base64,/,''), "base64"), function() {
            res.end();
            console.log("It's saved");
            next();
        });
    });
}
server.post('/submitImage',uploadFile);*/

//io.set("log level", 1); 

/*var bruno = Models.User({
  name: 'bruno',
  user: 'bruno',
  mail: 'bruno@saldivar.com.ar',
  password: 'elpulenta'
});

bruno.save();*/
// server.get(config.version + '/notes',routes.Notes.getAll);
// server.get(config.version + '/notes/:userName',routes.Notes.getByUserName);
server.listen(3000,function(){
  console.log('Ready on %s', server.url);
});