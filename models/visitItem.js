var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.ObjectId;

var visitItem = mongoose.model('visitItem', new mongoose.Schema(
{
    
	chromeId : String,
    urlId: { type: ObjectId, ref: 'URL'},
    userId: { type: ObjectId, ref: 'User'},
    referringVisitId: String,
    transition: String,
    visitId: String,
    visitTime: Number,
    userAgent : String,
    dateCreated : { type: Date, default:  Date.now },
    dateDeleted :  { type: Date , default:  null },
    deleted:	{ type: Boolean, default: 0 },
}));

exports.visitItem = visitItem;

// id:"89665"
// referringVisitId:"0"
// transition:"link"
// visitId:"228640"
// visitTime:1490960941711.354
