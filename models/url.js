var mongoose = require('mongoose');
//mongoose.Promise = require('bluebird');
var ObjectId = mongoose.Schema.ObjectId;


var URL = mongoose.model('URLs', new mongoose.Schema(
{
	chormeId : Number,
    url: String,
    title: String,
    favIconUrl: String,
    lastVisitTime: Number,
    lastVisitDate: Date, //=> esto deberia ir por usuario
    visitCount: Number,
    typedCount: Number,
    visitItems: [{ type: ObjectId, ref: 'visitItem'},],
    tags: [String],
    shortUrl: String,
    img: String,
    //stars:	[Number], //{number : Number} 
    dateCreated : { type: Date, default:  Date.now },
    dateDeleted :  { type: Date , default:  null},
    deleted:	{ type: Boolean, default: 0 },
}));


exports.url = URL;

