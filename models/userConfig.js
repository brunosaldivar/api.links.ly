
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.ObjectId;

var userConfig =  mongoose.model('usersConfigures', new mongoose.Schema(
{
    
    userId: { type: ObjectId, ref: 'user'},
    userAgent           :   String,
    ip  	            :   String,
    lastConnection      :  { type: Date, default:  Date.now },
    lastAddHistory   :    { type: Date,default: null}

}));

exports.userConfig = userConfig;