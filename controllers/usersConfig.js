var mongoose = require('mongoose');  
var userConfig = require('../models/userConfig').userConfig;  


exports.userConfig = (function(){

    return {
      
      save : function(data, callback) {
           
           var usrConf = new userConfig({
                userId          :   data.userId,
                lastAddHistory  :   data.lastAddHistory ,
                ip              :   data.ip,
                userAgent       :   data.userAgent,
            });

            usrConf.save(function(err, uc){
                if(err){
                    callback(err,null);}
                else{callback(null, uc);}
            });
      }   
    }
})();