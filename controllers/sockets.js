
var socketIO = require('socket.io');
var socketioJwt = require('socketio-jwt');
var config = require('../config');  

exports.SocketIO = (function() {
	//se usaria redis para guardar el sock.id con el id de usuario
	var clients 	= [];	//poner un fake con [sock.id, {usuario}]
	var io 			= {};
	return {

		Init 	 	: 		function(server){
		    
		    var socket = socketIO.listen(server);
		    io = socket;
		    io.set("transports", ["xhr-polling","websocket"]);
		    //io.set("log level", 1);
/*            io.set('authorization', socketioJwt.authorize({
                secret: config.token,
                handshake: true
            }));
*/
		    io.sockets.on('connection',socketioJwt.authorize({
                secret: config.token,
                timeout: 15000 // 15 seconds to send the authentication message
            })).on('authenticated', function(socket) {
                console.log(socket.handshake.decoded_token, ': connected');
		    	clients.push(sock.id);
            }).on('disconnect', function () {
                clients.splice(sock.id);
            });
		
		},
		Emit 	 	: 		function(all, id, caption, msg){

			if(all){
				console.log(clients);
				for (var i = 0; i < clients.length; i++) {
					var sId = clients[i];
					//este if lo agregue despues de tantos errores, ver q onda
					if(io.sockets.sockets[sId] !== undefined){
						io.sockets.sockets[sId].emit(caption, msg); 		
					}
				};
			}
			//io.sockets.sockets[sock.id].emit('connected', "vos sos: " + sock.id); 
		},
	}
})();

