var controllers = require('./controllers/index.js');

	exports.Auth	= (function (){
			return {
				login : function (req, res,next){
					
					controllers.authentication.login(req,res,next,function(err, data){
						if(err){
								res.end(JSON.stringify(err));
							}else{
								res.end(JSON.stringify(data));
							}
					});
					return next();
				}
			};
	})();

	exports.Chrome	= (function (){
		return {
			getHistories : function (req, res,next){
				controllers.chrome.getGroupHistoriesByDate('',function(err, data){
					if(err){
							res.end(JSON.stringify(err));
						}else{
							res.end(JSON.stringify(data));
						}
				});
				return next();
			},

			addHistory : function (req, res, next){
				
				var historyItems = req.params.items;
				var userId = req.params.userId;
				//aca tengo el req.userInfoRequest
				
				var userAgent = req.headers['user-agent'];
				var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
				
				var userData = {userId: userId, userAgent: userAgent, ip: ip};
				console.log(userData)
				controllers.chrome.addHistory(historyItems,userData, function(err, data){
					if(err){
							res.end(JSON.stringify(err));
						}else{
							res.end(JSON.stringify(data));
						}
				});
				return next();
			},
		}	
	})();

	// exports.Notes	= (function (){
	// 	return {
	// 		createNoteWithLinks : function (req, res, next){
	// 				var nt = req.params;
	// 				controllers.notes.saveNoteLinks(nt, function(err, data){
	// 					if(err){
	// 						res.end(JSON.stringify(err));
	// 					}else{
	// 						res.end(JSON.stringify(data));
	// 					}
	// 				});
	// 				return next();
	// 			}

	// 		// getAll : function (req, res, next){
	// 		// 		var nt = req.params;
	// 		// 		NotesController.saveNoteLinks(nt, function(err, data){
	// 		// 			if(err){
	// 		// 				res.end(JSON.stringify(err));
	// 		// 			}else{
	// 		// 				res.end(JSON.stringify(data));
	// 		// 			}
	// 		// 		});
	// 		// 		return next();
	// 		// 	},
	// 	})();

	 exports.Links	= (function (){
	 	return {

	 		getAll : function (req, res, next){
	 				var nt = req.params;
	 				controllers.url.getAll(function(err, data){
	 					if(err){
	 						res.end(JSON.stringify(err));
	 					}else{
	 						res.end(JSON.stringify(data));
	 					}
	 				});
	 				return next();
	 			}
	 	}
	 })();
