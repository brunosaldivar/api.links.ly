
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.ObjectId;

var UserModel =  mongoose.model('User', new mongoose.Schema(
{
    mail    : String,
    user  	: String,
    name	: String,
    password	: String,
    dateCreated : { type: Date, default:  Date.now },
    dateDeleted :  { type: Date },
    deleted:	{ type: Boolean, default: 0 },
}));

exports.User = UserModel;