var Models = require('../models/index.js');
var shortid = require('shortid');
var aut = require('./authentication');
var visitItemController = require('./visitIem').VisitItem;
var userConfigController = require('./usersConfig').userConfig;
var urlController = require('./url').URLController;
var q = require('q');

exports.ChromeController  = (function (){

		function porUrl( h ){
			return new Promise(function(resolve, reject){

				Models.url.findOne({'url': h.url}).then(function(data){
				
					if(!data){
						var ht =  new Models.url({
							chromeId : parseInt( h.id),
							url: h.url,
							title:	h.title,
							lastVisitTime:	h.lastVisitTime,
							lastVisitDate: new Date(h.lastVisitTime),
							visitCount:	h.visitCount,
							typedCount: h.typedCount,
							shortUrl: shortid.generate(),
							tags:	h.tags,
						});	
						
						ht.save(function(err){
							console.log(err)
							if(err)
								reject(err);
							resolve(ht);
						});
					}
				}).catch(function(err) {console.log(err)});
				
			});
		}
	return {
		getGroupHistoriesByDate : function (query, callback){

				Models.url.aggregate([
				{
					$project: {
						d: {$dayOfMonth: "$lastVisitDate"},
						m: {$month: "$lastVisitDate"},
						y: {$year: "$lastVisitDate"},

					}
				},
				{
					$group: {
							_id: {
								"day": "$d",
								"month": "$m",
								"year": "$y",
							},
						count: {$sum: 1}
					}
				}, {
					$sort: {
						"_id.year": -1,
						"_id.month": -1,
						"_id.day": -1,
					}
				} ], function (err, result) {
					callback(err,result);
				});
		},
		addHistory 	: 	function (history,userData, callback){
 			//try{
				 
				//la primer vez guardo el config, si no lo traigo pero en otro metodo
/*				var userConfig = Models.userConfig({
					 userId: 		userInfo.userId,
    				 userAgents: 	userInfo.userAgent,
					 ip	: 			userInfo.ip,
					 lastVisit: Date.now(),
    			     lastHistoryCharge: Date.now(),
				});
				console.log(userConfig)
				userConfig.save();*/
				console.log(history.length)
				console.log('history.length')
				history.reduce(function(sequence, historyItem) {
					return porUrl(historyItem);
				}).then(function(result) {
					console.log('sfdsdfs')
				}).catch(function (e) {
					console.log('erro2')
					console.log(e)});
			//});

			/*	console.log("Procesando: "  + history.length + " urls");

				history.forEach(function(h){

					Models.url.findOne({'url': h.url}, function (err,data){
					
					console.log("url: " + h.url);

					if(err){ callback(err, null); }
					if(!data){
						var ht =  new Models.url({
							chromeId : parseInt( h.id),
							url: h.url,
							title:	h.title,
							lastVisitTime:	h.lastVisitTime,
							lastVisitDate: new Date(h.lastVisitTime),
							visitCount:	h.visitCount,
							typedCount: h.typedCount,
							shortUrl: shortid.generate(),
							tags:	h.tags,
						});	
						ht.save(function(err){
							if(err){ callback(err, null); }
							console.log('Url creada: ' + ht.url + ' (' + h.visitItems.length + ')');
							if(h.visitItems instanceof Array){
								for (var i = 0; i < h.visitItems.length; i++) {
									var item = h.visitItems[i];
									item.urlId = ht._id;
									visitItemController.save(item,userData.userAgent, function(err){
										if(err){ callback(err, null); }
										ht.visitItems.push(itemData._id);
										ht.save(function(err){
											if(err){ callback(err,ht); }

										});
									});
								}
							}
						});
					}*/
					/*	if(data){
							if(h.visitItems instanceof Array){
								console.log('Url existente: ' + data + ' (' + h.visitItems.length + ')');
								for (var i = 0; i < h.visitItems.length; i++) {
									var item = h.visitItems[i];
									item.urlId = data._id;
									visitItemController.save(item,userData.userAgent, function(err, itemData){
										if(err){ callback(err, null); }
										data.visitItems.push(itemData._id);
										data.save(function(err,d){
											if(err){ callback(err, null); }

										});
									});
								}
							}
							
						}else{
							
							console.log('asasdfasdfasdf')
							var ht =  new Models.url({
								chromeId : parseInt( h.id),
								url: h.url,
								title:	h.title,
								lastVisitTime:	h.lastVisitTime,
								lastVisitDate: new Date(h.lastVisitTime),
								visitCount:	h.visitCount,
								typedCount: h.typedCount,
								shortUrl: shortid.generate(),
								tags:	h.tags,

							});	
							ht.save(function(err,data){
								if(err){ callback(err, null); }
								if(h.visitItems instanceof Array){
									console.log('Url creada: ' + data.url + ' (' + h.visitItems.length + ')');
									for (var i = 0; i < h.visitItems.length; i++) {
										var item = h.visitItems[i];
										item.urlId = data._id;
										visitItemController.save(item,userData.userAgent, function(err, itemData){
											if(err){ callback(err, null); }
											ht.visitItems.push(itemData._id);
											ht.save(function(err,d){
												if(err){ callback(err, null); }

											});
										});
									}
								}
							});	 
						}*/
							
				//	});
			//	});					
				/*userData.lastAddHistory = Date.now;
				userConfigController.save(userData, callback);
			}catch (e)
			{
				console.log('Errroorororoorororo!')
				console.log(e)
				cbk(e,null);

			}*/
		},
	}
})();