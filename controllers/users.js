var mongoose = require('mongoose');  
var User = require('../models/user').User;  
var config = require('../config');  
var jwt    = require('jsonwebtoken');
var bcrypt = require('bcrypt');

var moment = require('moment');


exports.UsersController = (function(){

    return {
      
      comparePassword : function(candidatePassword, user, callback) {
           
           bcrypt.compare(candidatePassword, user.password, function(err, isMatch) {
                if (err) return callback(err);
                //console.log({isMatch : isMatch, user: user});
                var rtn = {'isMatch' : isMatch, 'user': user};

                callback (err, rtn);
            });
            
      },
      find : function( username, callback) {
      
        User.findOne({ name: username }, function (err, user) {
            
            if (err) { return callback(err); }

            if (!user) {
              

                return callback(null, false);
            }
            callback(null, user);
          });
      },
    };

})();