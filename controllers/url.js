var shortid = require('shortid');
var models = require('../models');  

exports.URLController  = (function (){
	return {
		getAllPaging : function (options){
			return new Promise(function(resolve, reject){
				models.url.find({}).sort({'dateCreated': -1})
					.skip(options.skip)
					.limit(options.limit).exec( 
					function(err, result) {
						if(err){
							return reject(err)
						}
						resolve(result);	
					});
				});
		},
		getAllCount : function (callback){
			 models.url.count({}, function(err, result) {
					callback(err,result);	
				}
			);
		},
		getAll : function (options, callback){
			 models.url.find({}).sort({'dateCreated': -1}).limit(options.limit).exec( 
				function(err, result) {
					callback(err,result);	
				}
			);
		},
		getByURL : function (Url,userConfig, callback){

			Url.find({'url': Url}, function (err, result) {
				callback(err,result);
			});
			
		},
		addVisit : function (Url,userConfig, callback){

			
			
		},
		//le pasa el modelo 
		add	:	function(Url, callback){

			var u = new models.url(Url);
			console.log(u);
			u.save(
				function(err, data)
			    {
			    	callback(null, data);
				});
		},
		     
	}
})();