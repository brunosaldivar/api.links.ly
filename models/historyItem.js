var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.ObjectId;

var HistoryItem = mongoose.model('historyItem', new mongoose.Schema(
{
	id : String,
    url: String,
    title: String,
    lastVisitTime: Number,
    visitCount: Number,
    typedCount: Number,
    
    dateCreated : { type: Date, default:  Date.now },
    dateDeleted :  { type: Date , default:  null},
    deleted:	{ type: Boolean, default: 0 },
}));

exports.HistoryItem = HistoryItem;

