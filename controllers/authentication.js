
var users = require('./users').UsersController;  
var config = require('../config');  
var jwt    = require('jsonwebtoken');
var mongoose = require('mongoose');  
var UserConfig = require('../models/userConfig').userConfig;

exports.AuthController = (function(){

    return {
      wsValidate  :function(socket, next){
        
        if (socket.handshake.query && socket.handshake.query.token){
          jwt.verify(socket.handshake.query.token, config.token,  function(err, decoded) {      
            if (err) {
              console.log(err)
              return next(new Error('Authentication error'));
            } else {

              //socket.sockets[socket.id].emit('connected', "vos sos: " + sock.id); 
              next();
            }
          });
        }else{
          next(new Error('Authentication error'));
        }
        //console.log('paso ' )
        //next(new Error('Authentication error'));
      },
      validate  : function (req,res,next){

              var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        
              console.log('IP => ' + ip + ' is trying to connect');

              if(req.route.path == '/login')
              {
                return next();
              }
              var token = req.params.token; ///=> se acuerda con el cliente mandarlo ahi
              //console.log(token);
              if (token) {

                jwt.verify(token, config.token,  function(err, decoded) {      
               
                  if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });    
                  } else {
                    /*req.decoded = decoded;
                    console.dir( decodreq.ued);*/
                    return next();
                  }
                });

              } else {
                return res.send({ 
                    success: false, 
                    message: 'No token provided.' 
                });
                
              }

      },
      login : function (req,res,next){
         
          var username = ''; 
          var password = '';
           
          if( req.params ) {
            password = (req.body.password || req.params.password);
            username = (req.body.username || req.params.username);
          }
          
          /*else{
            if( req.headers["token"] ) {
              password = req.params.password;
              username = req.params.username;
            }
          
          console.log('cabeceras:');
          console.log(req.params);
          }*/
          users.find(username,function(err, user){
            if(!err)
            {
              
                users.comparePassword(password, user, function(err, data){
                
                  if(!err){
                    console.log(data.isMatch)
                   if(data.isMatch){
                        //Save y devolver config
                         //ESTO ES SI VIENE DE EXTENSION POR WEB NO SE HACE Y CELU NOSE 
                        UserConfig.findOne({ userId: user._id },['lastAddHistory'], {sort : {lastConnection : -1 }},
                          function(err, dc){
                           
                            if(err){ 
                              
                              res.send({ 
                                success: false, 
                                message: err 
                              });
                              return next(); 
                            } 
                            //console.log(user.id)
                            var usrConf = new UserConfig({

                              userId : user.id,
                              //lastConnection : Date.now,
                              lastAddHistory : null ,
                              ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
                              userAgent   :   req.headers['user-agent'],

                            });

                            usrConf.save(function(err, uc){
                                console.log(err);
                                req.token = jwt.sign({
                                  id: user.id
                                }, config.token, {
                                  expiresIn: config.tokenExpires
                                });
                                res.end(JSON.stringify({
                                  user: user.id,
                                  token: req.token,
                                  lastAddHistory : dc.lastAddHistory
                                }));
                            });
                            
                        });
                   }
                  else{
                      res.end(JSON.stringify({'isMatch': false}));
                    }
                  }
                  else{
                    res.end(JSON.stringify(err));
                  }
                  
                });
                
            }else{
              res.end(JSON.stringify(err));
            }

         //   return next();

          });
          
          
     
      },

    };

})();
